## General

JetBeep SDK version 0.5.

AAR file located here: app/libs/jetbeep-sdk.aar

In this repository you can find a sample project with JetBeep SDK being already setup

## Setup SDK in your Application class

```kotlin
class DemoApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        // 1. Initialize application
        JetBeepSDK.setApplication(this)

        // 2. If necessary, enable background mode
        JetBeepSDK.enableBackground()
    }
}
```

## Manage data and subscription to events from SDK in your activity\fragment
```kotlin
    private lateinit var repositoryReceiver: RepositoryEventReceiver
    private lateinit var jetbeepReceiver: EventReceiver

    override fun onResume() {
        super.onResume()

        // Request data from repository
        JetBeepSDK.requestOffersAsync()
        JetBeepSDK.requestLoyaltyCardsAsync()
        JetBeepSDK.requestShopsAsync()

        // Events from network and cache:
        repositoryReceiver = object : RepositoryEventReceiver(this) {
            override fun onHandleEvent(event: RepositoryEvent) {
                when (event) {
                    is OnOffersRetrieved -> {
                        val listOfOffers = event.data // List<Offer>: barcode, image, price, oldPrice,
                        // shopId, shopImageUrl, title, vendor, startDate, endDate
                    }
                    is OnLoyaltyCardsRetrieved -> {
                        val listOfLoyaltyCards = event.data // List<LoyaltyCard>: cardNumber, shopId,
                    }
                    is OnShopsRetrieved -> {
                        val listOfShops = event.data // List<Shop>: image, name
                    }
                }
            }
        }

        // Events from bluetooth:
        jetbeepReceiver = object: EventReceiver(this) {
            override fun onHandleEvent(event: BeeperEvent) {
                when (event) {
                    is OnShopEntered -> {
                        // this event is fired every time user enters the shop (detected by JetBeep device)
                        // it will not be fired more than once during the shop visit (at least 20 minutes,
                        // while JetBeep device is "OFF")

                        val shop = event.shop // Shop: id, name, image
                    }
                    is OnSuccess -> {
                        // this event is fired when loyalty card is successfully transferred
                        // triggered by JetBeepSDK.startBeep()
                    }
                    is OnError -> {
                        // this event is fired when an error occured during transferring loyalty card
                        // e.g. bluetooth connection lost or system error
                    }
                    is OnEmptyCard -> {
                        // this event is fired every time user tries to transfer non-existing loyalty card
                    }
                }
            }
        }

        jetbeepReceiver.subscribe()
        repositoryReceiver.subscribe()
    }

    override fun onPause() {
        super.onPause()

        jetbeepReceiver.unsubscribe()
        repositoryReceiver.unsubscribe()
    }
}
```

## Manually start\stop beeping

```kotlin
button3.setOnClickListener({
    if (JetBeepSDK.isBeeping) {
        // passing true will automatically turn OFF bluetooth adapter if it was enabled in startBeep
        JetBeepSDK.stopBeep(true)
        button3.text = "Start Beep"
    }
    else {
        // passing true will automatically turn ON bluetooth adapter if it's currently disabled
        JetBeepSDK.startBeep(true)
        button3.text = "Stop Beep"
    }
})
```
