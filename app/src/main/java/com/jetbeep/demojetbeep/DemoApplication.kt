package com.jetbeep.demojetbeep

import android.app.Application
import com.jetbeep.JetBeepSDK

class DemoApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        // 1. Initialize application
        JetBeepSDK.setApplication(this)

        // 2. If necessary, enable background mode
//        JetBeepSDK.enableBackground()
    }
}
